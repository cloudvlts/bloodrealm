package org.shouthost.bloodrealm.registry;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.material.Material;
import org.shouthost.bloodrealm.blocks.*;
import org.shouthost.bloodrealm.core.BloodRealm;

public class BRBlocks {

	public static void initBlocks(){
		BloodRealm.instance.bloodyGrass = new BloodyGrass();
		BloodRealm.instance.bloodyStone = new BloodyStone();
		BloodRealm.instance.bloodyCobbleStone = new BloodyCobbleStone();
		BloodRealm.instance.bloodyDirt = new BloodyDirt();
		BloodRealm.instance.bloodyAltarBlocks = new AltarStone();
	}

	public static void registerBlocks(){
		GameRegistry.registerBlock(BloodRealm.instance.bloodyGrass, BloodRealm.instance.bloodyGrass.getUnlocalizedName());
		GameRegistry.registerBlock(BloodRealm.instance.bloodyStone, BloodRealm.instance.bloodyStone.getUnlocalizedName());
		GameRegistry.registerBlock(BloodRealm.instance.bloodyCobbleStone, BloodRealm.instance.bloodyCobbleStone.getUnlocalizedName());
		GameRegistry.registerBlock(BloodRealm.instance.bloodyDirt, BloodRealm.instance.bloodyDirt.getUnlocalizedName());
		GameRegistry.registerBlock(BloodRealm.instance.bloodyAltarBlocks, BloodRealm.instance.bloodyAltarBlocks.getUnlocalizedName());
		GameRegistry.registerBlock(BloodRealm.instance.blood, BloodRealm.instance.blood.getUnlocalizedName());

	}

	public static void registerRecipes(){

	}

}
