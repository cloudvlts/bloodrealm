package org.shouthost.bloodrealm.registry;


import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.events.BlockCheck;
import org.shouthost.bloodrealm.events.CanDestroy;
import org.shouthost.bloodrealm.events.WorldEvents;
import org.shouthost.bloodrealm.utils.WorldHelper;

public class BREvents {

	public static void registerEvents(){
		BloodRealm.instance.destroyEvent = new CanDestroy(BloodRealm.instance.canDestroyInWorld);
		BloodRealm.instance.check = new BlockCheck();
		BloodRealm.instance.worldevents = new WorldEvents();
	}
}
