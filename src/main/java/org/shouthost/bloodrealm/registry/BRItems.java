package org.shouthost.bloodrealm.registry;


import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import org.shouthost.bloodrealm.armor.dragonarmor.DragonBootArmor;
import org.shouthost.bloodrealm.armor.dragonarmor.DragonChestPlateArmor;
import org.shouthost.bloodrealm.armor.dragonarmor.DragonHelmetArmor;
import org.shouthost.bloodrealm.armor.dragonarmor.DragonLeggingArmor;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.items.*;

import java.util.ArrayList;

public class BRItems {

	public static ArrayList<Item> list = new ArrayList<Item>();

	public static void initItems() {
		BloodRealm.instance.bloodBucket = new BloodBucket();
		BloodRealm.instance.dragonScales = new DragonScales();
		BloodRealm.instance.dragonHelmetArmor = new DragonHelmetArmor(0,0);
		BloodRealm.instance.dragonChestArmor = new DragonChestPlateArmor(1,1);
		BloodRealm.instance.dragonLeggingArmor = new DragonLeggingArmor(2,2);
		BloodRealm.instance.dragonBootArmor = new DragonBootArmor(3,3);
	}

	public static void registerItems(){
		GameRegistry.registerItem(BloodRealm.instance.bloodBucket, BloodRealm.instance.bloodBucket.getUnlocalizedName());
		GameRegistry.registerItem(BloodRealm.instance.dragonScales, BloodRealm.instance.dragonScales.getUnlocalizedName());
		GameRegistry.registerItem(BloodRealm.instance.dragonHelmetArmor, BloodRealm.instance.dragonHelmetArmor.getUnlocalizedName());
		GameRegistry.registerItem(BloodRealm.instance.dragonChestArmor, BloodRealm.instance.dragonChestArmor.getUnlocalizedName());
		GameRegistry.registerItem(BloodRealm.instance.dragonLeggingArmor, BloodRealm.instance.dragonLeggingArmor.getUnlocalizedName());
		GameRegistry.registerItem(BloodRealm.instance.dragonBootArmor, BloodRealm.instance.dragonBootArmor.getUnlocalizedName());
	}

	public static void registerRecipes(){
		//mainly for the armor and possibly any tools made along side
	}
}
