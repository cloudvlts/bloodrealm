package org.shouthost.bloodrealm.registry;


import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import org.shouthost.bloodrealm.blocks.FluidBlood;
import org.shouthost.bloodrealm.core.BloodRealm;

public class BRFluids {

	public static void initFluids(){
		BloodRealm.instance.bloodFluid = new Fluid("blood");
	}

	public static void registerFluids(){
		FluidRegistry.registerFluid(BloodRealm.instance.bloodFluid);
		BloodRealm.instance.blood = new FluidBlood();
		BloodRealm.instance.bloodFluid.setUnlocalizedName(BloodRealm.instance.blood.getUnlocalizedName());
		FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack("blood", FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(BloodRealm.instance.bloodBucket), new ItemStack(Items.bucket));
	}

}
