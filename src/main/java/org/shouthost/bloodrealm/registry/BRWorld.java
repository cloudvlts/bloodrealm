package org.shouthost.bloodrealm.registry;

import net.minecraftforge.common.DimensionManager;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.worlds.biomes.BiomeGenBlood;
import org.shouthost.bloodrealm.worlds.wprovider.BloodRealmWorldProvider;

public class BRWorld {

	public static void registerBiomes(){
		BloodRealm.instance.bloodBiome = new BiomeGenBlood(BloodRealm.instance.bloodyBiomeID);


	}

	public static void registerWorld(){
		DimensionManager.registerProviderType(BloodRealm.instance.worldId, BloodRealmWorldProvider.class, true);
		DimensionManager.registerDimension(BloodRealm.instance.worldId,BloodRealm.instance.worldId);
	}

	public static void unregisterWorld(){
		DimensionManager.unregisterDimension(BloodRealm.instance.worldId);
		DimensionManager.unloadWorld(BloodRealm.instance.worldId);
		DimensionManager.unregisterProviderType(BloodRealm.instance.worldId);
	}

}
