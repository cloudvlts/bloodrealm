package org.shouthost.bloodrealm.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.registry.BRWorld;
import org.shouthost.bloodrealm.utils.WorldHelper;

import java.io.File;

/**
 * Created by Darius on 5/19/2014.
 */
public class CommandReset extends CommandBase {
	@Override
	public String getCommandName() {
		return "reset";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return null;
	}

	@Override
	public void processCommand(ICommandSender iCommandSender, String[] strings) {
		final ICommandSender sender = iCommandSender;
		final boolean[] isFinished = new boolean[1];
		for(EntityPlayer player: BloodRealm.playerCache){
			if(player.worldObj.provider.dimensionId == BloodRealm.instance.worldId){
				WorldHelper.transferPlayerToWorld((EntityPlayerMP) player,WorldHelper.getWorldById(0));
			}
		}
		BRWorld.unregisterWorld();
		new Thread(){
			@Override
			public void run(){
				File file = new File(MinecraftServer.getServer().worldServerForDimension(BloodRealm.instance.worldId).getChunkSaveLocation(), "region");
				if(file.exists() && file.isDirectory()) {
					for(String f : file.list()){
						File region = new File(file, f);
						BloodRealm.instance.debug("Removing "+f);
						if(region.exists())
							region.delete();
					}
				}else{
					sender.addChatMessage(new ChatComponentText("Cant delete world"));
				}
				isFinished[0] = true;
			}
		}.start();

		//Attempt to load up the world to generate chunk 0,0 as a world ref
		//if this returns false, restart server asap
		while(isFinished[0] != true){
			//just making a blocking statement while waiting.
			if(isFinished[0] == true) break;
		}

		if(!WorldHelper.getWorldById(BloodRealm.instance.worldId).getChunkProvider().loadChunk(0,0).isChunkLoaded) return;
		iCommandSender.addChatMessage(new ChatComponentText(""+WorldHelper.getWorldById(BloodRealm.instance.worldId).getWorldInfo().getWorldName()+" been resetted"));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		BRWorld.registerWorld();

	}
}
