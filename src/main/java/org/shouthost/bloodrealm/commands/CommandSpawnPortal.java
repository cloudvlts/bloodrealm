package org.shouthost.bloodrealm.commands;

import net.minecraft.block.Block;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import org.shouthost.bloodrealm.core.BloodRealm;

/**
 * Created by Darius on 5/15/2014.
 */
public class CommandSpawnPortal extends CommandBase {
	private World world;
	private EntityPlayerMP player;

	@Override
	public String getCommandName() {
		return "spawnportal";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return null;
	}

	@Override
	public void processCommand(ICommandSender iCommandSender, String[] strings) {
		player = (EntityPlayerMP) iCommandSender;
		world = player.worldObj;
		int x = (int) player.posX;
		int y = (int) player.posY;
		int z = (int) player.posZ;
		//lets update the player coords before placing blocks
		player.setPositionAndUpdate(x+2,y,z+2);
		player.addChatMessage(new ChatComponentText("Player Position: " + x + "," + y + "," + z));
		//placeBlock(x, y + 1, z, BloodRealm.instance.bloodyAltarBlocks);
		placeBlock(x, y, z, BloodRealm.instance.bloodyStone);
		//gold ring'


		placeBlock(x + 1, y, z + 1, Blocks.gold_block);
		placeBlock(x,     y, z + 1, Blocks.gold_block);
		placeBlock(x - 1, y, z + 1, Blocks.gold_block);

		placeBlock(x + 1, y, z    , Blocks.gold_block);
		placeBlock(x - 1, y, z    , Blocks.gold_block);

		placeBlock(x - 1, y, z - 1, Blocks.gold_block);
		placeBlock(x,     y, z - 1, Blocks.gold_block);
		placeBlock(x + 1, y, z - 1, Blocks.gold_block);

		//bloody stone
		placeBlock(x + 2, y, z + 2, BloodRealm.instance.bloodyStone);
		placeBlock(x + 1, y, z + 2, BloodRealm.instance.bloodyStone);
		placeBlock(x ,    y, z + 2, BloodRealm.instance.bloodyStone);
		placeBlock(x - 1, y, z + 2, BloodRealm.instance.bloodyStone);
		placeBlock(x - 2, y, z + 2, BloodRealm.instance.bloodyStone);

		placeBlock(x + 2, y, z + 1, BloodRealm.instance.bloodyStone);
		placeBlock(x - 2, y, z + 1, BloodRealm.instance.bloodyStone);

		placeBlock(x + 2, y, z    , BloodRealm.instance.bloodyStone);
		placeBlock(x - 2, y, z    , BloodRealm.instance.bloodyStone);

		placeBlock(x + 2, y, z - 1, BloodRealm.instance.bloodyStone);
		placeBlock(x - 2, y, z - 1, BloodRealm.instance.bloodyStone);

		placeBlock(x + 2, y, z - 2, BloodRealm.instance.bloodyStone);
		placeBlock(x + 1, y, z - 2, BloodRealm.instance.bloodyStone);
		placeBlock(x    , y, z - 2, BloodRealm.instance.bloodyStone);
		placeBlock(x - 1, y, z - 2, BloodRealm.instance.bloodyStone);
		placeBlock(x - 2, y, z - 2, BloodRealm.instance.bloodyStone);

		//obsidian + corner blood stone
		placeBlock(x + 3, y, z + 3, Blocks.quartz_block);
		placeBlock(x + 2, y, z + 3, Blocks.obsidian);
		placeBlock(x + 1, y, z + 3, Blocks.obsidian);
		placeBlock(x ,    y, z + 3, Blocks.obsidian);
		placeBlock(x - 1, y, z + 3, Blocks.obsidian);
		placeBlock(x - 2, y, z + 3, Blocks.obsidian);
		placeBlock(x - 3, y, z + 3, Blocks.quartz_block);

		placeBlock(x + 3, y, z + 2, Blocks.obsidian);
		placeBlock(x - 3, y, z + 2, Blocks.obsidian);

		placeBlock(x + 3, y, z + 1, Blocks.obsidian);
		placeBlock(x - 3, y, z + 1, Blocks.obsidian);

		placeBlock(x + 3, y, z    , Blocks.obsidian);
		placeBlock(x - 3, y, z    , Blocks.obsidian);

		placeBlock(x + 3, y, z - 1, Blocks.obsidian);
		placeBlock(x - 3, y, z - 1, Blocks.obsidian);

		placeBlock(x + 3, y, z - 2, Blocks.obsidian);
		placeBlock(x - 3, y, z - 2, Blocks.obsidian);

		placeBlock(x + 3, y, z - 3, Blocks.quartz_block);
		placeBlock(x + 2, y, z - 3, Blocks.obsidian);
		placeBlock(x + 1, y, z - 3, Blocks.obsidian);
		placeBlock(x ,    y, z - 3, Blocks.obsidian);
		placeBlock(x - 1, y, z - 3, Blocks.obsidian);
		placeBlock(x - 2, y, z - 3, Blocks.obsidian);
		placeBlock(x - 3, y, z - 3, Blocks.quartz_block);

		//lets build up the pillars - Obsidian
		//corner one
		placeBlock(x + 3, y+1, z + 3, Blocks.obsidian);
		placeBlock(x + 3, y+2, z + 3, Blocks.obsidian);
		placeBlock(x + 3, y+3, z + 3, Blocks.quartz_block);

		//corner two
		placeBlock(x - 3, y+1, z + 3, Blocks.obsidian);
		placeBlock(x - 3, y+2, z + 3, Blocks.obsidian);
		placeBlock(x - 3, y+3, z + 3, Blocks.quartz_block);

		//corner three
		placeBlock(x + 3, y+1, z - 3, Blocks.obsidian);
		placeBlock(x + 3, y+2, z - 3, Blocks.obsidian);
		placeBlock(x + 3, y+3, z - 3, Blocks.quartz_block);

		//corner four
		placeBlock(x - 3, y+1, z - 3, Blocks.obsidian);
		placeBlock(x - 3, y+2, z - 3, Blocks.obsidian);
		placeBlock(x - 3, y+3, z - 3, Blocks.quartz_block);

	}

	void placeBlock(int x, int y, int z, Block block) {
		if (!world.setBlock(x, y, z, block)) return;
		BloodRealm.instance.debug("Placed " + block.getLocalizedName() + " at x" + x + " y" + y + " z" + z);
	}
}
