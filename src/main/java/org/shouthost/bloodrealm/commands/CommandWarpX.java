package org.shouthost.bloodrealm.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.utils.WorldHelper;

import javax.swing.text.html.parser.Entity;

public class CommandWarpX extends CommandBase {
	@Override
	public String getCommandName() {
		return "warpx";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return EnumChatFormatting.RED + "/warpx [world | dim = 0]";
	}

	@Override
	public void processCommand(ICommandSender iCommandSender, String[] args) {
        //check to see if player can goto that world
		World realm = WorldHelper.getWorldById(BloodRealm.instance.worldId);
		EntityPlayer player = (EntityPlayer) iCommandSender;
        if(WorldHelper.doesWorldExist(realm) &&
                realm.getChunkProvider().loadChunk(0,0).isChunkLoaded){
            boolean check = WorldHelper.transferPlayerToWorld((EntityPlayerMP) iCommandSender, realm);
            if(!check) WorldHelper.transferPlayerToWorld((EntityPlayerMP) iCommandSender, WorldHelper.getWorldById(0));
        }
	}

}
