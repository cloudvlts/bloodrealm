package org.shouthost.bloodrealm.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

/**
 * Created by Darius on 5/20/2014.
 */
public class CommandTNT extends CommandBase {
	@Override
	public String getCommandName() {
		return "tnt";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return null;
	}

	@Override
	public void processCommand(ICommandSender iCommandSender, String[] strings) {
		World world = iCommandSender.getEntityWorld();
		int x = iCommandSender.getPlayerCoordinates().posX;
		int y = iCommandSender.getPlayerCoordinates().posY;
		int z = iCommandSender.getPlayerCoordinates().posZ;

		for(int i = 0;i<20;i++){
			for(int f = 0;f < 20;f++){
				for(int g = 0;g < 20;g++){
					world.setBlock(x+i, y+f, z+g, Blocks.tnt);
				}
			}
		}


	}
}
