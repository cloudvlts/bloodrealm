package org.shouthost.bloodrealm.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import org.shouthost.bloodrealm.core.BloodRealm;

/**
 * Created by Darius on 5/15/2014.
 */
public class CommandCheck extends CommandBase {

	@Override
	public String getCommandName() {
		return "check";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender) {
		return "";
	}

	@Override
	public void processCommand(ICommandSender iCommandSender, String[] strings) {
		EntityPlayerMP player = (EntityPlayerMP) iCommandSender;
		if (!BloodRealm.instance.canCheckBlock.contains(player.getUniqueID()) && BloodRealm.instance.canCheckBlock != null) {
            BloodRealm.instance.debug("Attempting to add to list");
            BloodRealm.instance.canCheckBlock.add(player.getUniqueID());
		} else {
            BloodRealm.instance.debug("Attempting to remove from list");
            BloodRealm.instance.canCheckBlock.remove(player.getUniqueID());
		}
	}
}
