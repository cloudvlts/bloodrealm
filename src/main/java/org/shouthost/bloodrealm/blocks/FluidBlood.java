package org.shouthost.bloodrealm.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntitySquid;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import org.shouthost.bloodrealm.core.BloodRealm;

public class FluidBlood extends BlockFluidClassic {

	@SideOnly(Side.CLIENT)
	public static IIcon bloodStillIcon;
	@SideOnly(Side.CLIENT)
	public static IIcon bloodFlowingIcon;

	public FluidBlood() {
		super(BloodRealm.instance.bloodFluid, Material.water);
		setBlockName("BloodFluid");
		setCreativeTab(CreativeTabs.tabMisc);

	}


	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		if (!world.isRemote) {
//			if (entity instanceof EntitySquid){
//				((EntitySquid) entity).setDead();
//			}
//			if (entity instanceof EntityLivingBase) {
//				((EntityLivingBase) entity).addPotionEffect(new PotionEffect(Potion.weakness.id, 500));
//				((EntityLivingBase) entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 500));
//			}
		}
	}

	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {
		bloodStillIcon = iconRegister.registerIcon("bloodrealm:blood_still");
		bloodFlowingIcon = iconRegister.registerIcon("bloodrealm:blood_flow");
	}

	@Override
	public IIcon getIcon(int side, int meta) {
		return side != 0 && side != 1 ? bloodFlowingIcon : bloodStillIcon;
	}
}
