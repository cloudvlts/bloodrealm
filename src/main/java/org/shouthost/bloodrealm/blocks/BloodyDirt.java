package org.shouthost.bloodrealm.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import org.shouthost.bloodrealm.core.BloodRealm;

public class BloodyDirt extends Block {
	public BloodyDirt() {
		super(Material.grass);
		setBlockName("BloodyDirt");
        this.setHardness(0.6F);
        this.setHarvestLevel("shovel", 0);
        this.setTickRandomly(true);
        this.setStepSound(Block.soundTypeGrass);
		setCreativeTab(CreativeTabs.tabMisc);
		setBlockTextureName("bloodrealm:bloodydirt");
	}

	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
		if (entity instanceof EntityPlayer) {
			((EntityPlayer) entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 1000));
			((EntityPlayer) entity).addPotionEffect(new PotionEffect(Potion.poison.id, 50, 20));
		}
//		super.onEntityWalking(world, x, y, z, entity);
	}

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta) {
		int j1 = meta & 3;
		byte b0 = 0;
		BloodRealm.instance.debug("Block "+ world.getBlock(x,y,z).getLocalizedName());
		if(world.getBlock(x,y-1,z) == Blocks.grass){
			BloodRealm.instance.debug("Replacing to dirt");
			world.setBlockToAir(x,y-1,z);
			world.setBlock(x,y-1,z,Blocks.dirt);
		}else if(world.getBlock(x,y-1,z) == BloodRealm.instance.bloodyGrass){
			BloodRealm.instance.debug("Replacing to bloodydirt");
			world.setBlockToAir(x,y-1,z);
			world.setBlock(x,y-1,z,BloodRealm.instance.bloodyDirt);
		}

		return j1 | b0;
	}
}
