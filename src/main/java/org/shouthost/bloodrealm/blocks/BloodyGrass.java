package org.shouthost.bloodrealm.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import org.shouthost.bloodrealm.core.BloodRealm;

import java.util.Random;

public class BloodyGrass extends Block {

	@SideOnly(Side.CLIENT)
	protected IIcon blockIconTop;

	@SideOnly(Side.CLIENT)
	protected IIcon blockIcon;

	@SideOnly(Side.CLIENT)
	protected IIcon blockIconBotton;

	public BloodyGrass() {
		super(Material.grass);
		setBlockName("BloodyGrass");
		setCreativeTab(CreativeTabs.tabMisc);
        this.setHardness(0.6F);
        this.setHarvestLevel("shovel", 0);
        this.setTickRandomly(true);
        this.setStepSound(Block.soundTypeGrass);
		//setBlockTextureName("bloodrealm:bloodystone");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon) {
		blockIcon = icon.registerIcon("bloodrealm:bloodygrass_side");
		blockIconTop = icon.registerIcon("bloodrealm:bloodygrass_top");
		blockIconBotton = icon.registerIcon("bloodrealm:bloodydirt");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int metadata) {
		if (side == 1) {
			return blockIconTop;
		} else if (side == 6) {
			return blockIconBotton;
		} else {
			return blockIcon;
		}
	}

    @Override
    //TODO: 	randomDisplayTick()
    public void randomDisplayTick(World world, int x, int y, int z, Random random)
    {
        if (!world.isRemote)
            return;
        if (world.getBlockMetadata(x, y, z) == 0)
        {
            if (random.nextInt(4) == 0)
            {
                world.spawnParticle("smoke", x + random.nextFloat(), y + 1.1F, z + random.nextFloat(), 0.0D, 0.0D, 0.0D);
            }
        }
    }

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta) {
		int j1 = meta & 3;
		byte b0 = 0;
		if(world.getBlock(x,y-1,z) == Blocks.grass){
			world.setBlockToAir(x,y-1,z);
			world.setBlock(x,y-1,z,Blocks.dirt);
		}else if(world.getBlock(x,y-1,z) == BloodRealm.instance.bloodyGrass){
			world.setBlockToAir(x,y-1,z);
			world.setBlock(x,y-1,z,BloodRealm.instance.bloodyDirt);
		}

		return j1 | b0;
	}

	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int meta) {
		if(world.getBlock(x,y,z) == BloodRealm.instance.bloodyGrass){
			world.getBlock(x,y,z).dropBlockAsItem(world,x,y,z,world.getBlockMetadata(x,y,z),0);
		}
	}

}
