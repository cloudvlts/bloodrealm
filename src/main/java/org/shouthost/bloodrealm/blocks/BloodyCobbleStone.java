package org.shouthost.bloodrealm.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BloodyCobbleStone extends Block {
	public BloodyCobbleStone() {
		super(Material.ground);
		this.setBlockName("BloodyCobbleStone");
		this.setCreativeTab(CreativeTabs.tabMisc);
		this.setBlockTextureName("bloodrealm:bloodycobblestone");
        this.setHardness(0.6F);
        this.setHarvestLevel("pickaxe", 0);
        this.setTickRandomly(true);
        this.setStepSound(Block.soundTypeStone);
	}
}
