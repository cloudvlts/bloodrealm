package org.shouthost.bloodrealm.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.utils.WorldHelper;

import java.util.*;

public class AltarStone extends Block {

	public HashMap<World, UUID> playerlist = new HashMap<World, UUID>();
	public ArrayList<Block> altarblocks = new ArrayList<Block>();
	public World world;
	@SideOnly(Side.CLIENT)
	protected IIcon blockIconSides;

	@SideOnly(Side.CLIENT)
	protected IIcon blockIcon;

	public AltarStone() {
		super(Material.iron);
		setBlockName("AltarStone");
		setCreativeTab(CreativeTabs.tabMisc);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon) {
		blockIconSides = icon.registerIcon("bloodrealm:altar_side");
		blockIcon = icon.registerIcon("bloodrealm:altar_common");
	}

	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z) {
		world.addWeatherEffect(new EntityLightningBolt(world, x,y,z));
		return super.removedByPlayer(world, player, x, y, z);
	}

	@Override
	public Block setResistance(float p_149752_1_) {
		return super.setResistance(p_149752_1_);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int metadata) {
		if (side == 1) {
			return blockIcon;
		} else if (side == 6) {
			return blockIconSides;
		} else {
			return blockIconSides;
		}
	}

	@Override
	public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
		if (entity instanceof EntityPlayer) {
			((EntityPlayer) entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 1000));
			world.addWeatherEffect(new EntityLightningBolt(entity.worldObj, x, y, z));
		} else {
			if (!(entity instanceof EntityEnderman)) {
				world.addWeatherEffect(new EntityLightningBolt(entity.worldObj, x, y, z));
				entity.setDead();
			}
		}
		super.onEntityWalking(world, x, y, z, entity);
	}

	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
		if (!world.isRemote)
			if (entity instanceof EntityLivingBase)
				((EntityLivingBase) entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 50));
	}

//	@Override
//	public void updateTick(World world, int x, int y, int z, Random random){
//
//	}

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta) {
		if(altarCheck(world,x,y-1,z)){
			world.setBlockToAir(x,y-1,z);
			world.setBlock(x, y - 1, z, BloodRealm.instance.blood);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			world.createExplosion(null,x,y,z,5f,false);
			ArrayList<EntityLightningBolt> lightning = new ArrayList<EntityLightningBolt>();
			lightning.add(new EntityLightningBolt(world, x + 3, y+3, z + 3));
			lightning.add(new EntityLightningBolt(world, x - 3, y+3, z + 3));
			lightning.add(new EntityLightningBolt(world, x + 3, y+3, z - 3));
			lightning.add(new EntityLightningBolt(world, x - 3, y+3, z - 3));


		}
		return 0;
	}

	public boolean altarCheck(World world, int x, int y, int z){
		this.world = world;
		if(checkBlock(x, y, z, BloodRealm.instance.bloodyStone)&&
			checkBlock(x + 1, y, z + 1, Blocks.gold_block) &&
			checkBlock(x,     y, z + 1, Blocks.gold_block) &&
			checkBlock(x - 1, y, z + 1, Blocks.gold_block)  &&

			checkBlock(x + 1, y, z    , Blocks.gold_block) &&
			checkBlock(x - 1, y, z    , Blocks.gold_block) &&

			checkBlock(x - 1, y, z - 1, Blocks.gold_block) &&
			checkBlock(x,     y, z - 1, Blocks.gold_block) &&
			checkBlock(x + 1, y, z - 1, Blocks.gold_block) &&

			//bloody stone
			checkBlock(x + 2, y, z + 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x + 1, y, z + 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x ,    y, z + 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 1, y, z + 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 2, y, z + 2, BloodRealm.instance.bloodyStone) &&

			checkBlock(x + 2, y, z + 1, BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 2, y, z + 1, BloodRealm.instance.bloodyStone) &&

			checkBlock(x + 2, y, z    , BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 2, y, z    , BloodRealm.instance.bloodyStone) &&

			checkBlock(x + 2, y, z - 1, BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 2, y, z - 1, BloodRealm.instance.bloodyStone) &&

			checkBlock(x + 2, y, z - 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x + 1, y, z - 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x    , y, z - 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 1, y, z - 2, BloodRealm.instance.bloodyStone) &&
			checkBlock(x - 2, y, z - 2, BloodRealm.instance.bloodyStone) &&

			//obsidian + corner blood stone
			checkBlock(x + 3, y, z + 3, Blocks.quartz_block) &&
			checkBlock(x + 2, y, z + 3, Blocks.obsidian) &&
			checkBlock(x + 1, y, z + 3, Blocks.obsidian) &&
			checkBlock(x ,    y, z + 3, Blocks.obsidian) &&
			checkBlock(x - 1, y, z + 3, Blocks.obsidian) &&
			checkBlock(x - 2, y, z + 3, Blocks.obsidian) &&
			checkBlock(x - 3, y, z + 3, Blocks.quartz_block) &&

			checkBlock(x + 3, y, z + 2, Blocks.obsidian) &&
			checkBlock(x - 3, y, z + 2, Blocks.obsidian) &&

			checkBlock(x + 3, y, z + 1, Blocks.obsidian) &&
			checkBlock(x - 3, y, z + 1, Blocks.obsidian) &&

			checkBlock(x + 3, y, z    , Blocks.obsidian) &&
			checkBlock(x - 3, y, z    , Blocks.obsidian) &&

			checkBlock(x + 3, y, z - 1, Blocks.obsidian) &&
			checkBlock(x - 3, y, z - 1, Blocks.obsidian) &&

			checkBlock(x + 3, y, z - 2, Blocks.obsidian) &&
			checkBlock(x - 3, y, z - 2, Blocks.obsidian) &&

			checkBlock(x + 3, y, z - 3, Blocks.quartz_block) &&
			checkBlock(x + 2, y, z - 3, Blocks.obsidian) &&
			checkBlock(x + 1, y, z - 3, Blocks.obsidian) &&
			checkBlock(x ,    y, z - 3, Blocks.obsidian) &&
			checkBlock(x - 1, y, z - 3, Blocks.obsidian) &&
			checkBlock(x - 2, y, z - 3, Blocks.obsidian) &&
			checkBlock(x - 3, y, z - 3, Blocks.quartz_block) &&

			//lets build up the pillars - Obsidian
			//corner one
			checkBlock(x + 3, y+1, z + 3, Blocks.obsidian) &&
			checkBlock(x + 3, y+2, z + 3, Blocks.obsidian) &&
			checkBlock(x + 3, y+3, z + 3, Blocks.quartz_block) &&

			//corner two
			checkBlock(x - 3, y+1, z + 3, Blocks.obsidian) &&
			checkBlock(x - 3, y+2, z + 3, Blocks.obsidian) &&
			checkBlock(x - 3, y+3, z + 3, Blocks.quartz_block) &&

			//corner three
			checkBlock(x + 3, y+1, z - 3, Blocks.obsidian) &&
			checkBlock(x + 3, y+2, z - 3, Blocks.obsidian) &&
			checkBlock(x + 3, y+3, z - 3, Blocks.quartz_block) &&

			//corner four
			checkBlock(x - 3, y+1, z - 3, Blocks.obsidian) &&
			checkBlock(x - 3, y+2, z - 3, Blocks.obsidian) &&
			checkBlock(x - 3, y+3, z - 3, Blocks.quartz_block)){
			return true;
		}
		return false;
	}

	public boolean checkBlock(int x,int y, int z, Block block){
		if (world.getBlock(x, y, z) == block) return true;
		else return false;
	}


}
