package org.shouthost.bloodrealm.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BloodyStone extends Block {
	public BloodyStone() {
		super(Material.ground);
		setBlockName("BloodyStone");
		setCreativeTab(CreativeTabs.tabMisc);
		setBlockTextureName("bloodrealm:bloodystone");
        this.setHardness(0.6F);
        this.setHarvestLevel("pickaxe", 0);
        this.setTickRandomly(true);
        this.setStepSound(Block.soundTypeStone);
	}
}
