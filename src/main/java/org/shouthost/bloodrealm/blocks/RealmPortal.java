package org.shouthost.bloodrealm.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import java.util.Random;

/**
 * Created by Darius on 5/15/2014.
 */
public class RealmPortal extends Block {

	public RealmPortal(){
		super(Material.portal);
		this.setBlockName("Realm Portal");
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z) {
		return null;
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

	@Override
	public int quantityDropped(Random rand) {
		return 0;
	}

	@Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity) {
/*		if (entity.ridingEntity == null && entity.riddenByEntity == null && entity instanceof EntityPlayerMP) {
			EntityPlayerMP player = (EntityPlayerMP) entity;
			if (player.dimension != BloodRealm.instance.worldId) {
				player.timeUntilPortal = 3;
				player.mcServer.getConfigurationManager().transferPlayerToDimension(player, BloodRealm.instance.worldId, new BloodRealmTeleport(player.mcServer.worldServerForDimension(BloodRealm.instance.worldId)));
			}
		}*/
		if(entity instanceof EntityPlayerMP){
			EntityPlayerMP player = (EntityPlayerMP) entity;
			world.addWeatherEffect(new EntityLightningBolt(world, x,y,z));
			player.addChatMessage(new ChatComponentText("You are not welcome in my world"));
		}

	}
}
