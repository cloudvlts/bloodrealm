package org.shouthost.bloodrealm.client;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderHerobrine extends RenderBiped {
	public ResourceLocation texture;
	public RenderHerobrine(ModelBiped model) {
		super(model, 0.5f);
	}

	@Override
	public ResourceLocation getEntityTexture(Entity entity){
		return  new ResourceLocation("bloodrealm:herobrine");
	}
}
