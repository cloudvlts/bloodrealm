package org.shouthost.bloodrealm.proxy;

import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.client.model.ModelBiped;
import org.shouthost.bloodrealm.client.RenderHerobrine;
import org.shouthost.bloodrealm.entity.EntityHerobrine;

public class ClientProxy extends CommonProxy {
	@Override
	public void registerEntities() {
		RenderingRegistry.registerEntityRenderingHandler(EntityHerobrine.class, new RenderHerobrine(new ModelBiped()));
	}
}
