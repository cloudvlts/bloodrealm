package org.shouthost.bloodrealm.fluids;

import net.minecraftforge.fluids.Fluid;
import org.shouthost.bloodrealm.blocks.FluidBlood;

public class BloodFluid extends Fluid {
	public BloodFluid(String fluidName) {
		super(fluidName);
		this.setIcons(FluidBlood.bloodStillIcon, FluidBlood.bloodFlowingIcon);
	}
}
