package org.shouthost.bloodrealm.armor.dragonarmor;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemArmor;

public class DragonLeggingArmor extends ItemArmor {
	public DragonLeggingArmor(int p_i45325_2_, int p_i45325_3_) {
		super(ArmorMaterial.DIAMOND, p_i45325_2_, p_i45325_3_);
		this.setTextureName("bloodrealm:dragon_leggings");
		this.setCreativeTab(CreativeTabs.tabCombat);
		this.setUnlocalizedName("DragonLeggings");
	}
}
