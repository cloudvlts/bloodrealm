package org.shouthost.bloodrealm.armor.dragonarmor;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.ISpecialArmor;
import org.shouthost.bloodrealm.core.BloodRealm;

public class DragonHelmetArmor extends ItemArmor implements ISpecialArmor{
	public DragonHelmetArmor(int p_i45325_2_, int p_i45325_3_) {
		super(ArmorMaterial.DIAMOND, p_i45325_2_, p_i45325_3_);
		this.setTextureName("bloodrealm:dragon_helmet");
		this.setCreativeTab(CreativeTabs.tabCombat);
		this.setUnlocalizedName("DragonHelmet");
	}

	@Override
	public ISpecialArmor.ArmorProperties getProperties(EntityLivingBase player, ItemStack armor, DamageSource source, double damage, int slot) {
		if(source == source.inFire || source == source.lava || source == source.onFire) { //Check for fire damage, you can use other types of damage too.
			return new ISpecialArmor.ArmorProperties(1, 1, MathHelper.floor_double(damage * .25D));
		}
		return new ISpecialArmor.ArmorProperties(0, 0, 0);
	}

	@Override
	public int getArmorDisplay(EntityPlayer player, ItemStack armor, int slot) {
		return 4;
	}

	@Override
	public void damageArmor(EntityLivingBase entity, ItemStack stack, DamageSource source, int damage, int slot) {
		stack.damageItem(damage * 2, entity);
	}

	@Override
	public String getArmorTexture(ItemStack armor, Entity entity, int slot, String type) {
		if(armor.getItem() == BloodRealm.instance.dragonHelmetArmor) {
			return "bloodrealm:dragon_layer1";
		} else {
			return "bloodrealm:dragon_layer2";
		}
	}

	@Override
	public CreativeTabs[] getCreativeTabs() {
		return new CreativeTabs[] {CreativeTabs.tabCombat}; //This lets me put my armor in as many create tabs as I want, pretty cool right?
	}

	@Override
	public boolean getIsRepairable(ItemStack armor, ItemStack stack) {
		return false;
	}
}


