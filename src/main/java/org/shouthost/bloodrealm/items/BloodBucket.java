package org.shouthost.bloodrealm.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.ItemFluidContainer;

public class BloodBucket extends ItemFluidContainer {

	@SideOnly(Side.CLIENT)
	protected IIcon bloodBucket;

	public BloodBucket() {
		super(0, FluidContainerRegistry.BUCKET_VOLUME);
		this.setUnlocalizedName("BloodBucket");
		this.setContainerItem(Items.bucket);
		this.maxStackSize = 1;
		this.setCreativeTab(CreativeTabs.tabMisc);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
		MovingObjectPosition movingobjectposition = this.getMovingObjectPositionFromPlayer(world, player, false);

		if (movingobjectposition != null) {
			int i = movingobjectposition.blockX;
			int j = movingobjectposition.blockY;
			int k = movingobjectposition.blockZ;

			if (movingobjectposition.sideHit == 0) {
				--j;
			}

			if (movingobjectposition.sideHit == 1) {
				++j;
			}

			if (movingobjectposition.sideHit == 2) {
				--k;
			}

			if (movingobjectposition.sideHit == 3) {
				++k;
			}

			if (movingobjectposition.sideHit == 4) {
				--i;
			}

			if (movingobjectposition.sideHit == 5) {
				++i;
			}

			if (!player.canPlayerEdit(i, j, k, movingobjectposition.sideHit, itemStack)) {
				return itemStack;
			}
/*this.tryPlaceContainedLiquid(itemStack, world, i, j, k) &&*/
			if (!player.capabilities.isCreativeMode) {
				return new ItemStack(Items.bucket);
			}
		}

		return itemStack;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister iconRegister) {
		bloodBucket = iconRegister.registerIcon("bloodrealm:bucket_blood");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(ItemStack itemStack, int renderPass) {
		FluidStack fluid = this.getFluid(itemStack);

		if (fluid != null && fluid.amount != 0) {
			return bloodBucket;
		}

		return Items.bucket.getIconFromDamage(0);
	}


}
