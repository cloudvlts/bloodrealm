package org.shouthost.bloodrealm.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Created by Darius on 5/15/2014.
 */
public class DragonScales extends Item {
	public DragonScales() {
		super();
		this.setHasSubtypes(false);
		this.setUnlocalizedName("DragonScales");
		this.setCreativeTab(CreativeTabs.tabMisc);
		this.setTextureName("bloodrealm:dragon_scales");
	}

}
