package org.shouthost.bloodrealm.utils;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import org.shouthost.bloodrealm.worlds.transport.BloodRealmFakeTeleporter;
import org.shouthost.bloodrealm.worlds.transport.BloodRealmTeleport;

public class WorldHelper {
	public static int getIdFromWorld(World world) {
		return world.provider.dimensionId;
	}

	public static String getWorldName(World world) {
		return world.getWorldInfo().getWorldName();
	}

	public static Boolean transferPlayerToWorld(EntityPlayerMP player, World world) {
		MinecraftServer.getServer().getConfigurationManager().transferPlayerToDimension(player, getIdFromWorld(world), new BloodRealmFakeTeleporter(player.mcServer.worldServerForDimension(world.provider.dimensionId)));
		if (getIdFromWorld(player.worldObj) == getIdFromWorld(world)) return true;
		return false;
	}

	public static World getWorldById(int id) {
		return MinecraftServer.getServer().worldServerForDimension(id);
	}

	public static World getWorldByName(String name) {
		for (WorldServer w : getWorldList())
			if (w.getWorldInfo().getWorldName().contains(name))
				return w;
		return null;
	}

	public static WorldServer[] getWorldList() {
		return MinecraftServer.getServer().worldServers;
	}

	//dummy function for now
	public static boolean doesWorldExist(World world) {
		return true;
	}

}
