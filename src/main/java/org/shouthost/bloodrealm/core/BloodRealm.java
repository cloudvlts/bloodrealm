package org.shouthost.bloodrealm.core;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fluids.Fluid;
import org.shouthost.bloodrealm.commands.*;
import org.shouthost.bloodrealm.registry.*;
import org.shouthost.bloodrealm.events.BlockCheck;
import org.shouthost.bloodrealm.events.CanDestroy;
import org.shouthost.bloodrealm.events.WorldEvents;
import org.shouthost.bloodrealm.proxy.CommonProxy;

import java.util.ArrayList;
import java.util.UUID;

@Mod(name = BloodRealm.NAME, modid = BloodRealm.MODID, version = BloodRealm.VERSION)
public class BloodRealm {

	public static final String NAME = "BloodRealm";
	public static final String MODID = "bloodrealm";
	public static final String VERSION = "@VERSION@";

	//This will be used to list the players in the BloodRealm World
	public static ArrayList<UUID> list = new ArrayList<UUID>();
	public static ArrayList<UUID> canCheckBlock = new ArrayList<UUID>();
	public static ArrayList<EntityPlayer> playerCache = new ArrayList<EntityPlayer>();


	@SidedProxy(clientSide = "org.shouthost.bloodrealm.proxy.ClientProxy", serverSide = "org.shouthost.bloodrealm.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Instance
	public static BloodRealm instance;

	//Blocks

	public static Block bloodyGrass;
	public static Block bloodyStone;
	public static Block bloodyCobbleStone;
	public static Block bloodyDirt;
	public static Block bloodyAltarBlocks;
	public static Block hardenVoidBlocks;
	public static Block blood;

	//Fluids
	public static Fluid bloodFluid;

	//Items

	public static Item bloodBag;
	public static Item bloodyKnife;
	public static Item dragonScales;
	public static Item dragonHelmetArmor;
	public static Item dragonChestArmor;
	public static Item dragonLeggingArmor;
	public static Item dragonBootArmor;
	public static Item bloodBucket;


	//Entities ?

	//Tile Entity
	public static TileEntity dragonForge;
	//Biome
	public static BiomeGenBase bloodBiome;
	//Settings
	public Configuration config;
	public boolean debug; //for debugging the code with additional logging
	//World Information for Configuration
	public String worldType;
	public int worldId;
	public boolean BloodRealmWorld;
	public boolean canDestroyInWorld;
	public boolean canCheatInWorld;
	//misc
	public CanDestroy destroyEvent;
	public WorldEvents worldevents;
	public BlockCheck check;

	public int bloodyBiomeID;
	//Creative Tab
	CreativeTabs tab; // will create custom tab

	@EventHandler
	public void PreInit(FMLPreInitializationEvent event) {
		config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		debug = config.get(Configuration.CATEGORY_GENERAL, "debug", true).getBoolean(true);
		BloodRealmWorld = config.get("World Information", "EnableWorld", false).getBoolean(false);
		worldType = config.get("World Information", "worldType", "DEFAULT").getString();
		worldId = config.get("World Information", "worldId", 90).getInt(90);
		canCheatInWorld = config.get("World Information", "canCheatInWorld", true).getBoolean(true);
		canDestroyInWorld = config.get("World Information", "canDestroyInWorld", true).getBoolean(true);
		bloodyBiomeID = config.get("World Information", "BiomeID", 157).getInt(157);
		config.save();

		//Register Fluids
		BRFluids.initFluids();

		//register Blocks
		BRBlocks.initBlocks();

		//register Items
		BRItems.initItems();

		//Register Events
		BREvents.registerEvents();

		//Rendering
		proxy.registerEntities();

	}

	@EventHandler
	public void Init(FMLInitializationEvent event) {
		BRFluids.registerFluids();
		BRBlocks.registerBlocks();
		BRItems.registerItems();
		//if(bloodyBiomeID != 0)
			BRWorld.registerBiomes();
		//if(BloodRealmWorld)
			BRWorld.registerWorld();
        DimensionManager.unloadWorld(-1);
        DimensionManager.unregisterDimension(-1);
	}

	@EventHandler
	public void ServerStarting(FMLServerStartingEvent event) {
		if(debug){
			event.registerServerCommand(new CommandWarpX());
			event.registerServerCommand(new CommandCheck());
			event.registerServerCommand(new CommandSpawnPortal());
			event.registerServerCommand(new CommandReset());
			event.registerServerCommand(new CommandTNT());
		}
	}

	@EventHandler
	public void ServerStopping(FMLServerStoppingEvent event) {

	}

	public void debug(String message) {
		if (!debug) return;
		System.out.println(message);
	}
}
