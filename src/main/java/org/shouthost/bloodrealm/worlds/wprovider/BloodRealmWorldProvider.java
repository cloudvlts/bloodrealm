package org.shouthost.bloodrealm.worlds.wprovider;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.WorldChunkManager;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraftforge.client.IRenderHandler;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.worlds.cprovider.BloodRealmChunkProvider;
import org.shouthost.bloodrealm.worlds.sprovider.BloodRealmSkyProvider;

public class BloodRealmWorldProvider extends WorldProvider {

	@Override
	public void registerWorldChunkManager() {
		worldChunkMgr = new WorldChunkManager(this.worldObj);
		this.dimensionId = BloodRealm.instance.worldId;
		this.hasNoSky = false;
	}


	@Override
	public String getDimensionName() {
		return "Blood Realm";
	}

	@Override
	public boolean isSurfaceWorld()
	{
		return true;
	}

	@Override
	public ChunkCoordinates getEntrancePortalLocation() {
		return null;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public Vec3 getSkyColor(Entity cameraEntity, float partialTicks) {

		long time = this.worldObj.getWorldTime();
		float red = 1;
		float green = 1;
		// Green and blue use the same values in this instance

		// DAY
		if (time >= 500 && time <= 11500) {
			return this.worldObj.getWorldVec3Pool().getVecFromPool(0.6, 0.4, 0.4);
		}

		// NIGHT
		else if (time >= 14000 && time <= 22000) {
			return this.worldObj.getWorldVec3Pool().getVecFromPool(0, 0, 0);
		}

		else {
			//SUNRISE
			if (time > 22000 || time < 500) {

				// CREATE RANGE OF 1 - 2499
				if (time > 22000) {
					time = (time - 500) - 21500;
					// 1 -> 1999
				} else if (time < 500) {
					time = 2000 + time;
					// 2000 - 2499
				}
				red = (float) (0.00023999999999999998 * time);
				green = (float) (0.00016 * time);
			}


			//SUNSET
			else {
				red = (float) (-(0.00024019215372297838 * time) + 3.3624499599679742);
				green = (float) (-(0.0001601281024819856 * time) + 2.2416333066453165);
			}

			return this.worldObj.getWorldVec3Pool().getVecFromPool(red, green, green);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean isSkyColored() {
		return true;
	}

	@Override
	public IChunkProvider createChunkGenerator() {
		return new BloodRealmChunkProvider(worldObj, worldObj.getSeed(), true);
	}

	@Override
	public boolean canRespawnHere() {
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public Vec3 getFogColor(float par1, float par2) {
		float f2 = MathHelper.cos(par1 * (float) Math.PI * 2.0F) * 2.0F + 0.5F;

		if (f2 < 0.2F)
		{
			f2 = 0.2F;
		}

		if (f2 > 1.0F)
		{
			f2 = 1.0F;
		}

		float f3 = 0.7529412F;
		float f4 = 0.84705883F;
		float f5 = 1.0F;
		f3 *= f2 * 0.94F + 0.06F;
		f4 *= f2 * 0.94F + 0.06F;
		f5 *= f2 * 0.91F + 0.09F;
		f3 = 0.9F * f2;
		f4 = 0.75F * f2;
		f5 = 0.6F * f2;
		return this.worldObj.getWorldVec3Pool().getVecFromPool((double)f3, (double)f4, (double)f5);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean getWorldHasVoidParticles() {
		return true;
	}

	@Override
	public String getWelcomeMessage()
	{
		return "Entering BloodRealm - a land of no return";
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IRenderHandler getSkyRenderer() {
		return new BloodRealmSkyProvider();
	}

	@Override
	public String getDepartMessage()
	{
		return "Leaving BloodRealm";
	}

	@Override
	public boolean doesXZShowFog(int par1, int par2)
	{
		return false;
	}



}
