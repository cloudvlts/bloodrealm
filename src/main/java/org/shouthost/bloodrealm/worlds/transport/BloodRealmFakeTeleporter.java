package org.shouthost.bloodrealm.worlds.transport;

import net.minecraft.entity.Entity;
import net.minecraft.world.Teleporter;
import net.minecraft.world.WorldServer;

/**
 * Created by Darius on 5/19/2014.
 */
public class BloodRealmFakeTeleporter extends Teleporter {
	public BloodRealmFakeTeleporter(WorldServer worldServer) {
		super(worldServer);
	}

	@Override
	public boolean makePortal(Entity entity) {
		return false;
	}
}
