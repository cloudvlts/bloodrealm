package org.shouthost.bloodrealm.worlds.transport;

import net.minecraft.util.ChunkCoordinates;

/**
 * Created by Darius on 5/19/2014.
 */
public class BloodRealmPortalPosition extends ChunkCoordinates {
	final BloodRealmTeleport teleport;
	public BloodRealmPortalPosition(BloodRealmTeleport portal, int x, int y, int z){
		super(x,y,z);
		this.teleport = portal;
	}
}
