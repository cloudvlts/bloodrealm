package org.shouthost.bloodrealm.worlds.transport;

import net.minecraft.entity.Entity;
import net.minecraft.world.Teleporter;
import net.minecraft.world.WorldServer;

/**
 * Created by Darius on 5/15/2014.
 */
public class BloodRealmTeleport extends Teleporter {
	private final WorldServer world;

	public BloodRealmTeleport(WorldServer worldServer) {
		super(worldServer);
		this.world = worldServer;
	}

	@Override
	public void placeInPortal(Entity entity, double v, double v2, double v3, float v4) {
		super.placeInPortal(entity, v, v2, v3, v4);
	}

	@Override
	public boolean makePortal(Entity entity) {
		return false;
	}

	@Override
	public void removeStalePortalLocations(long l) {
		super.removeStalePortalLocations(l);
	}

	@Override
	public boolean placeInExistingPortal(Entity entity, double v, double v2, double v3, float v4) {
		return super.placeInExistingPortal(entity, v, v2, v3, v4);
	}
}
