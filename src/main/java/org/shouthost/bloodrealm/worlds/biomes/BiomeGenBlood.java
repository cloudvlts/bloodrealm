package org.shouthost.bloodrealm.worlds.biomes;

import net.minecraft.block.material.Material;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import org.shouthost.bloodrealm.core.BloodRealm;

import java.util.Random;

public class BiomeGenBlood extends BiomeGenBase {
	public final Material blockMaterial;

	public BiomeGenBlood(int par1) {
		super(par1);
		this.blockMaterial = Material.water;
		this.topBlock = BloodRealm.instance.bloodyGrass;
		this.fillerBlock = BloodRealm.instance.bloodyDirt;
		this.setBiomeName("Bloody Biome");
		this.waterColorMultiplier = 0xE42D17;
	}

	@Override
	public void decorate(World par1World, Random par2Random, int par3, int par4) {
		super.decorate(par1World, par2Random, par3, par4);
	}



	@Override
	public boolean canSpawnLightningBolt() {
		return true;
	}

	@Override
	public boolean getEnableSnow() {
		return false;
	}

}
