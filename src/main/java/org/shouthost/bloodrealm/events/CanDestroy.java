package org.shouthost.bloodrealm.events;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;
import org.shouthost.bloodrealm.core.BloodRealm;

public class CanDestroy {
	public boolean canDestroy;

	public CanDestroy(boolean destroy) {
		this.canDestroy = destroy;
		MinecraftForge.EVENT_BUS.register(this);
       // FMLCommonHandler.instance().bus().register(this);

    }

	@SubscribeEvent
	public void canDestroy(BlockEvent.BreakEvent event) {
		if (this.canDestroy) return;
		World world = event.getPlayer().worldObj;
		if (world == MinecraftServer.getServer().worldServerForDimension(BloodRealm.instance.worldId)) {
			event.setCanceled(true);
		}
	}
}
