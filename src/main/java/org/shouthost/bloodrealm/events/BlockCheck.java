package org.shouthost.bloodrealm.events;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.world.BlockEvent;
import org.shouthost.bloodrealm.core.BloodRealm;

/**
 * Created by Darius on 5/15/2014.
 */
public class BlockCheck {

	public BlockCheck() {
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onBlockBreak(PlayerInteractEvent event) {
		if (!BloodRealm.instance.canCheckBlock.contains(event.entityPlayer.getUniqueID())) return;

		event.entityPlayer.addChatMessage(new ChatComponentText("Block " + event.entityPlayer.worldObj.getBlock(event.x, event.y, event.z).getLocalizedName() + " at x" + event.x + " y" + event.y + " z" + event.z + ""));

		event.setCanceled(true);
	}

}
