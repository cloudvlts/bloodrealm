package org.shouthost.bloodrealm.events;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.LongHashMap;
import net.minecraft.world.World;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.event.world.WorldEvent;
import org.shouthost.bloodrealm.core.BloodRealm;
import org.shouthost.bloodrealm.utils.WorldHelper;

import java.util.ArrayList;

public class WorldEvents {


	public WorldEvents() {
        FMLCommonHandler.instance().bus().register(this);
    }

	@SubscribeEvent
	public void ChangeWorldEvent(PlayerEvent.PlayerChangedDimensionEvent event) {
		//Might add more checks just to be safe
//		if (event.player.worldObj == world ||
//				WorldHelper.getWorldByName(event.player.worldObj.getWorldInfo().getWorldName()) == world ||
//				BloodRealm.instance.list.contains(event.player.getUniqueID())) {
//			event.setCanceled(true);
//		}
	}

	@SubscribeEvent
	public void PlayerLoginEvent(PlayerEvent.PlayerLoggedInEvent event){
		if(BloodRealm.playerCache.contains(event.player)) return;
		BloodRealm.playerCache.add(event.player);
	}

	@SubscribeEvent
	public void PlayerLogoutEvent(PlayerEvent.PlayerLoggedOutEvent event){
		if(!BloodRealm.playerCache.contains(event.player)) return;
		BloodRealm.playerCache.remove(event.player);
	}

	@SubscribeEvent
	public void WorldUnloadEvent(WorldEvent.Unload event){

	}
}
