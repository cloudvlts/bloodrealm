package org.shouthost.bloodrealm.entity;

import com.sun.org.apache.xpath.internal.operations.Bool;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.world.World;

public class EntityHerobrine extends EntityCreature{

	private boolean canSpawn;
	private boolean canAttack;
	private boolean canStalk;
	private boolean canThrow;
	private double xPos;
	private double yPos;
	private double zPos;
	private EntityPlayer target;
	private String[] randomString = {};

	public EntityHerobrine(World par1World) {
		super(par1World);
		getNavigator().setCanSwim(false);
		getNavigator().setEnterDoors(false);
		setHealth(getMaxHealth());
		this.isImmuneToFire = true;
		this.experienceValue = 5;
		this.targetTasks.addTask(1, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));
	}

	public EntityHerobrine(World par1World, double x, double y, double z) {
		super(par1World);
		getNavigator().setCanSwim(true);
		getNavigator().setEnterDoors(false);
		setHealth(getMaxHealth());
		this.isImmuneToFire = true;
		this.experienceValue = 5;
		setPosition(x, y, z);
		this.targetTasks.addTask(1, new EntityAINearestAttackableTarget(this, EntityPlayer.class, 0, true));
	}

	@Override
	public void setTarget(Entity entity) {
		super.setTarget(entity);
	}



}
